package com.czxbnb.sdkdemo

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.chur.indoorlocation.data.LoginDataSource
import com.chur.indoorlocation.data.LoginRepository
import com.chur.indoorlocation.sdk.ChurSDK
import com.chur.indoorlocation.sdk.beacon.ChurLocationManager
import com.chur.indoorlocation.sdk.manager.PermissionManager
import com.chur.indoorlocation.sdk.model.user.LoggedInUser
import com.chur.indoorlocation.sdk.model.user.User
import com.chur.indoorlocation.sdk.preference.PreferenceUtils
import com.chur.indoorlocation.sdk.utils.permission.RequestCallback
import com.churinc.tymonlibrary.RxHttpUtils
import com.churinc.tymonlibrary.utils.ToastUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    @SuppressLint("InlinedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        PermissionManager.with(this).permissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
        ).request(object : RequestCallback() {
            override fun onAllGranted() {}

            override fun onDenied(
                deniedAndNextAskList: List<String>?,
                deniedAndNeverAskList: List<String>?
            ) {
            }
        })

        val churLocationManager = ChurLocationManager.getInstance(this)

        btn_login.setOnClickListener {
            RxHttpUtils.init(this)
            login(et_username.text.toString(), et_password.text.toString())
            btn_login.text = getString(R.string.logging_in)
            btn_login.isEnabled = false
        }

        btn_start.setOnClickListener {
            if (btn_start.text == "Start Service") {
                churLocationManager.startService("Demo App", "App is running at background")
                btn_start.text = "Stop Service"
            } else {
                churLocationManager.stopService()
                btn_start.text = "Start Service"
            }
        }
    }


    fun login(username: String, password: String) {
        // can be launched in a separate asynchronous job
        val loginRepository = LoginRepository(
            dataSource = LoginDataSource()
        )
        loginRepository.login(username, password, object : LoginDataSource.OnLoginCompleted {
            override fun onSuccess(user: LoggedInUser) {
                val user = User()

                user.businessId = PreferenceUtils.getInstance().businessId
                user.businessUserId = PreferenceUtils.getInstance().businessUserId
                user.username = PreferenceUtils.getInstance().username
                ChurSDK.setUser(user)
                ToastUtils.showLongToast("Login successful!")

                btn_start.visibility = View.VISIBLE
                et_username.visibility = View.GONE
                et_password.visibility = View.GONE
                btn_login.visibility = View.GONE
            }

            override fun onFailure(message: String) {
                ToastUtils.showLongToast(message)
                btn_login.text = getString(R.string.login)
                btn_login.isEnabled = true
            }
        })

    }
}