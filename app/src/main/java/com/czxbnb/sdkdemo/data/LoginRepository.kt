package com.chur.indoorlocation.data

import com.chur.indoorlocation.sdk.ChurSDK
import com.chur.indoorlocation.sdk.model.user.LoggedInUser
import com.chur.indoorlocation.sdk.preference.PreferenceUtils

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(val dataSource: LoginDataSource) {

    // in-memory cache of the loggedInUser object
    var user: LoggedInUser? = null
        private set

    val isLoggedIn: Boolean
        get() = user != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        user = null
    }

    fun logout() {
        user = null
        dataSource.logout()
    }

    fun login(username: String, password: String, listener: LoginDataSource.OnLoginCompleted){
        // handle login
        dataSource.login(username, password, object: LoginDataSource.OnLoginCompleted{
            override fun onFailure(message: String) {
               listener.onFailure(message)
            }

            override fun onSuccess(user: LoggedInUser) {
                setLoggedInUser(user)

                ChurSDK.setBusinessId(user.user_info.businessId)
                ChurSDK.setBusinessUserId(user.user_info.businessUserId)
                PreferenceUtils.getInstance().username = user.user_info.username
                listener.onSuccess(user)
            }
        })


    }

    private fun setLoggedInUser(loggedInUser: LoggedInUser) {
        this.user = loggedInUser
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }
}
